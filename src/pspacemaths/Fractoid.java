package pspacemaths;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.Random;
import org.apache.commons.math3.fraction.*;

/**
 * This class extends an Asteroid and defines a Fractoid. Which is basically a 
 * bigger asteroid that stores a fraction and displays it as text on the 
 * Fractoid when drawn
 * @author Kierran McPherson
 */
public class Fractoid extends Asteroid {

    /**
     * A String representation of the Fraction in the format of Numerator/Denominator
     */
    private String fraction;
    /**
     * An instance of the {@link Fraction} class used to store and calculate 
     * the Fraction
     */
    private Fraction question;
    /**
     * A double representing the rounded decimal value of the fraction
     */
    private double roundedAnswer;
    /**
     * The hitBox of the Fractoid used to detect collisions
     */
    private Rectangle hitBox;

    /**
     * Constructs a new Fractoid using a given image, starting position, 
     * horizontal speed, and difficulty. It calls the super constructor. And 
     * creates the {@link Fraction} representing the math problem.
     * @param image The image of the Fractoid
     * @param position The starting position of the Fractiod
     * @param xSpeed The Fractoid's horizontal speed
     * @param difficulty The difficulty of the Fractoids Fraction problem
     */
    public Fractoid(Image image, Point position, int xSpeed, int difficulty) {
       super(image, position, xSpeed);
        Random rand = new Random();
        int temp = rand.nextInt(difficulty) + 1;
        String ans = "0." + String.valueOf(temp);
        roundedAnswer = Double.parseDouble(ans);
        question = new Fraction(roundedAnswer, 100);
        updateFraction();
        hitBox = new Rectangle(position.x + 5, position.y + 5, (int) (image.getWidth(null) * 1.5) - 5, (int) (image.getHeight(null) * 1.5) - 5);
    }

    /**
     * Gets the string representation of the Fractiod's fraction
     * @return A string representation of the fraction
     */
    public String getFraction() {
        return fraction;
    }

    /**
     * Gets the Fractoid's question in the form of an instance of the
     * {@link Fraction} class 
     * @return The Fractoid's Fraction
     */
    public Fraction getQuestion() {
        return question;
    }

    /**
     * Updates the Fractoid's position by calling the super classes update
     * position. It then moves the hitBox to match.
     */
    @Override
    public void updatePosition() {
        super.updatePosition();
        hitBox.x = (int) getPosX();
        hitBox.y = (int) getPosY();
    }
    /**
     * Updates the Fractoid's string representation of the question
     */
    private void updateFraction() {
        fraction = question.getNumerator() + "/" + question.getDenominator();
    }

    /**
     * Gets the Fractoid's rounded answer
     * @return The double value that is the answer to this Fractoid's question
     */
    public double getRoundedAnswer() {
        return roundedAnswer;
    }

    /**
     * Gets the Fractoid's hitBox Rectangle
     * @return A rectangle the size of the Fractoid
     */
    public Rectangle getHitBox() {
        return hitBox;
    }

    /**
     * Subtracts the value of Fraction f from the Fractoid's question
     * @param f The Fraction you are taking off the question
     */
    public void subtractFromFraction(Fraction f) {
        question = question.subtract(f);
        updateFraction();
    }

    /**
     * Draws the Fractoid to the given Graphics2D object using the correct 
     * AffineTransform to scale the image and adds the fraction to the centre of
     * the Fractoid
     * @param buffer The Graphics2D object that the Fractoid is drawn too.
     */
    @Override
    public void draw(Graphics2D buffer) {
        AffineTransform af = new AffineTransform();
        af.translate(getPosX(), getPosY());
        af.scale(1.5, 1.5);
        buffer.drawImage(getImage(), af, null);
        buffer.setColor(Color.WHITE);
        buffer.setFont(new Font("Terminal", Font.BOLD, 24));
        buffer.drawString(fraction, hitBox.x + hitBox.width / 3, (int)hitBox.getCenterY());

    }
}
