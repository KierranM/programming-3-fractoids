package pspacemaths;

import java.awt.*;
import java.awt.geom.*;

/**
 * This class defines a Laser. Which is fired by a Ship against a Fractoid
 * @author Kierran McPherson
 */
public class Laser {
    /**
     * The X,Y position of the top left of the Laser
     */
    private Point2D position;
    /**
     * The Image of the Laser
     */
    private Image image;
    /**
     * The horizontal speed of the laser
     */
    private int xSpeed;
    /**
     * The power level of the Laser. Used to compare against the rounder answer of a Fractoid
     */
    private double powerLevel;
    /**
     * The rectangular hitBox used to check for collisions against other hitBoxes
     */
    private Rectangle hitBox;

    /**
     * THis constructs a new Laser given a position, image, and a power level
     * @param position The starting x,y position of the laser
     * @param image The Image of the Laser
     * @param powerLevel The power level of the Laser
     */
    public Laser(Point position, Image image, int powerLevel) {
        this.position = position;
        this.image = image;
        while(image.getWidth(null) < 0){
            System.out.println("Laser Image Loading");
        }
        hitBox = new Rectangle(position.x, position.y, image.getWidth(null)/2, image.getHeight(null));
        String temp = "0." + String.valueOf(powerLevel);
        this.powerLevel = Double.parseDouble(temp);
        xSpeed = 15;
    }
    
    /**
     * Draws the Laser to the specified Graphics2D object
     * @param buffer The Graphics2D object that the Laser is drawn to
     */
    public void draw(Graphics2D buffer){
        AffineTransform af = new AffineTransform();
            af.translate(getPosition().getX(), getPosition().getY());
            af.scale(1, 1);
            buffer.drawImage(getImage(), af, null);
    }

    /**
     * Gets the Laser's position
     * @return A Point2D representing the Laser's C,Y coordinates
     */
    public Point2D getPosition() {
        return position;
    }

    /**
     * Gets the Image of the Laser
     * @return The Image of the Laser
     */
    public Image getImage() {
        return image;
    }

    /**
     * Gets the power level of the Laser
     * @return The power level of the Laser as a double value
     */
    public double getPowerLevel() {
        return powerLevel;
    }

    /**
     * Sets the Laser's position Point to that of the given point
     * @param position The new position of the Laser
     */
    public void setPosition(Point position) {
        this.position = position;
    }
    
    /**
     * Updates the laser position by moving the Laser's position along the X axis
     * Then updates the hitBox to reflect the changes in position.
     */
    public void updatePosition(){
        position = new Point((int)position.getX() + xSpeed, (int)position.getY());
        hitBox.x = (int)position.getX();
        hitBox.y = (int)position.getY();
    }

    /**
     * Gets the Laser's hitBox
     * @return A Rectangle representing the hitBox of the Laser
     */
    public Rectangle getHitBox() {
        return hitBox;
    }
    
}
