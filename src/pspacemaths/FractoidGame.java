package pspacemaths;

import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.*;
import javax.swing.*;
import org.apache.commons.math3.fraction.*;

/**
 * This class contains the majority of the game logic. It loads all of the audio
 * and images, and updates all the different objects involved in the game
 */
public class FractoidGame implements KeyListener, MouseListener, MouseMotionListener, ActionListener {
    /**
     * The standard Font size used for the
     */
    private final int FONTSIZE = 54;
    /**
     * Represents the difficulty of the fractions that appear on the Fractoids
     */
    private final int EASY = 10;
    //private final int MEDIUM = 50;
    //private final int HARD = 100;
    //Unusable difficulty settings
    private ArrayList<Asteroid> asteroids;
    private ArrayList<Fractoid> fractoids;
    private ArrayList<Image> asteroidImages;
    private ArrayList<Laser> lasers;
    private Image background;
    private Image starField;
    private Image titleImage;
    private Image gameOver;
    private Image laserImage;
    private Image lifeImage;
    private BufferedImage offScreen;
    private Ship ship;
    private Screen currentScreen;
    private Graphics2D buffer;
    private AudioClip backgroundLoop;
    private AudioClip fireLaser;
    private AudioClip fractoidExplosion;
    private AudioClip badPower;
    private AudioClip shipCollision;
    private Rectangle playRectangle;
    private Rectangle playAgainRectangle;
    private Rectangle powerRectangle;
    private Rectangle healthRectangle;
    private Rectangle powerOutline;
    private Rectangle healthOutline;
    private Point titleXY;
    private Point playXY;
    private BitSet keysPressed;
    private double spaceMove;
    private double spaceX;
    private double spaceX1;
    private double starsMove;
    private double starsX;
    private double starsX1;
    private int countDown;
    private int count;
    private int lives;
    private int score;
    private int level;
    private boolean music;
    private boolean explosion;
    private boolean playMouseOver;
    private boolean playAgainMouseOver;
    private JApplet runningApplet;

    /**
     * THis creates a new instance of the {@link FractoidGame} given a reference
     * to the JApplet that created it
     * @param runningApplet The JApplet that created this instance
     */
    public FractoidGame(JApplet runningApplet) {
        this.runningApplet = runningApplet;
        //Initialise the ArrayLists
        asteroids = new ArrayList<>();
        asteroidImages = new ArrayList<>();
        fractoids = new ArrayList<>();
        lasers = new ArrayList<>();
        ArrayList<Image> shipImages = new ArrayList<>();
        //Load the asteroid Images
        for (int i = 0; i < 64; i++) {
            asteroidImages.add(runningApplet.getImage(runningApplet.getDocumentBase(), "asteroid" + i + ".png"));
        }
        //Load the laser image
        laserImage = runningApplet.getImage(runningApplet.getDocumentBase(), "laser.png");
        
        //Load the background images
        background = runningApplet.getImage(runningApplet.getDocumentBase(), "bg.gif");
        starField = runningApplet.getImage(runningApplet.getDocumentBase(), "ParallaxStars.png");
        
        //Load the extra images
        titleImage = runningApplet.getImage(runningApplet.getDocumentBase(), "title.png");
        gameOver = runningApplet.getImage(runningApplet.getDocumentBase(), "GameOver.png");
        lifeImage = runningApplet.getImage(runningApplet.getDocumentBase(), "lifeicon.png");
        
        //Load all the ship images
        for (int i = 0; i < 8; i++) {
            if (i == 0) {
                shipImages.add(runningApplet.getImage(runningApplet.getDocumentBase(), "ship.png"));
            } else {
                shipImages.add(runningApplet.getImage(runningApplet.getDocumentBase(), "ship" + i + ".png"));
            }
        }
        
        //Create the ship
        ship = new Ship(shipImages, new Point(30, 50));
        runningApplet.setSize(1000, 600);
        
        //Set up Grapohics options
        offScreen = new BufferedImage(1000, 600, 1);
        buffer = offScreen.createGraphics();
        buffer.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        buffer.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //Load audio
        backgroundLoop = runningApplet.getAudioClip(runningApplet.getDocumentBase(), "sfloop.au");
        fireLaser = runningApplet.getAudioClip(runningApplet.getDocumentBase(), "laser.au");
        fractoidExplosion = runningApplet.getAudioClip(runningApplet.getDocumentBase(), "boom.au");
        badPower = runningApplet.getAudioClip(runningApplet.getDocumentBase(), "badPower.au");
        shipCollision = runningApplet.getAudioClip(runningApplet.getDocumentBase(), "shipCollision.au");
        //Pause until all images are loaded
        while (imagesLoaded() == false) {
            runningApplet.showStatus("loading");
        }
        //Initialise background variables
        spaceX = 0;
        spaceX1 = background.getWidth(runningApplet) - 2;
        spaceMove = 0.05;
        starsMove = 0.3;
        starsX = 0;
        starsX1 = starField.getWidth(runningApplet);
        //Set up Points and Rectangles
        titleXY = new Point(500 - (titleImage.getWidth(runningApplet)) / 2, 50);
        playXY = new Point(titleXY.x, titleXY.y + titleImage.getHeight(runningApplet) + 100);
        playRectangle = new Rectangle(playXY.x, playXY.y - FONTSIZE, 110, FONTSIZE + 10);
        playAgainRectangle = new Rectangle(titleXY.x + 152, titleXY.y + gameOver.getHeight(runningApplet) + 208, 270, 54);
        powerRectangle = new Rectangle(240, 575, 50, 20);
        powerOutline = new Rectangle(240, 575, 500, 20);
        healthRectangle = new Rectangle(10, 60, 20, 500);
        healthOutline = new Rectangle(10, 60, 20, 500);
        
        //Set the current screen to the Main menu
        currentScreen = Screen.Main;
        
        //Begin the background music
        backgroundLoop.loop();
        music = true;
        //Initialise gameplay variables
        playMouseOver = false;
        playAgainMouseOver = false;
        count = 150;
        lives = 3;
        score = 0;
        level = 1;
        keysPressed = new BitSet(255);
    }
    
    /**
     * This method stops the background audio loop
     */
    public void stopMusic() {
        backgroundLoop.stop();
    }

    /**
     * This method checks if all the Images have been successfully loaded
     * @return If even a single image is not loaded then return false. Otherwise return True
     */
    public final boolean imagesLoaded() {
        boolean retBool = true;
        if (background.getWidth(runningApplet) < 0) {
            retBool = false;
        }
        if (starField.getWidth(runningApplet) < 0) {
            retBool = false;
        }
        if (titleImage.getWidth(runningApplet) < 0) {
            retBool = false;
        }

        if (gameOver.getWidth(runningApplet) < 0) {
            retBool = false;
        }

        if (lifeImage.getWidth(runningApplet) < 0) {
            retBool = false;
        }

        for (int i = 0; i < ship.numberOfImages; i++) {
            if (ship.getImage(i).getWidth(runningApplet) < 0) {
                retBool = false;
            }
        }
        for (int i = 0; i < asteroidImages.size(); i++) {
            if (asteroidImages.get(i).getWidth(runningApplet) < 0) {
                retBool = false;
            }
        }
        return retBool;
    }

    /**
     * This method draws all the individual components onto the offScreen Image
     * @return The offScreen Image containing all the drawn components
     */
    public BufferedImage draw() {
        drawBackground();
        drawAsteroids();

        if (currentScreen == Screen.Game) {
            drawGameScreen();
        } else {
            if (currentScreen == Screen.Main) {
                drawMainMenuScreen();
            } else {
                if (currentScreen == Screen.GameOver) {
                    drawGameOverScreen();
                }
            }
        }

        return offScreen;
    }
    
    /**
     * This method draws the background images onto the buffer using the correct
     * AffineTransform to scale them
     */
    public void drawBackground(){
        AffineTransform bg = new AffineTransform();
        AffineTransform bg1 = new AffineTransform();
        AffineTransform sf = new AffineTransform();
        AffineTransform sf1 = new AffineTransform();


        buffer.clearRect(0, 0, 1000, 600);
        bg.translate(spaceX, 0);
        buffer.drawImage(background, bg, runningApplet);
        bg1.translate(spaceX1, 0);
        buffer.drawImage(background, bg1, runningApplet);
        sf.translate(starsX, 0);
        buffer.drawImage(starField, sf, runningApplet);
        sf1.translate(starsX1, 0);
        buffer.drawImage(starField, sf1, runningApplet);

    }
    
    /**
     * Draws the Game over screen which displays the players highest level, 
     * score, and the play again button to the buffer
     */
    public void drawGameOverScreen(){
        buffer.setColor(new Color(50, 50, 50, 200));
                    buffer.fillRoundRect(20, 20, 960, 560, 50, 50);
                    buffer.drawImage(gameOver, titleXY.x, titleXY.y, runningApplet);
                    buffer.setFont(new Font("Terminal", Font.BOLD, 24));
                    buffer.setColor(Color.WHITE);
                    buffer.drawString("Your ship has been destroyed!", titleXY.x, titleXY.y + gameOver.getHeight(runningApplet) + 50);
                    buffer.drawString("Your final score was " + score, titleXY.x, titleXY.y + gameOver.getHeight(runningApplet) + 80);
                    buffer.drawString("You managed to reach level " + level, titleXY.x, titleXY.y + gameOver.getHeight(runningApplet) + 110);
                    if (playAgainMouseOver) {
                        buffer.setColor(Color.RED);
                    }
                    buffer.setFont(new Font("Terminal", Font.BOLD, FONTSIZE));
                    buffer.drawString("Play Again", titleXY.x + 150, titleXY.y + gameOver.getHeight(runningApplet) + 250);
    }
    
    /**
     * Draws the Main Menu to the buffer which includes the Play button and the instructions on how to play
     */
    public void drawMainMenuScreen(){
        
                buffer.setColor(new Color(50, 50, 50, 200));
                buffer.fillRoundRect(20, 20, 960, 560, 50, 50);
                buffer.drawImage(titleImage, titleXY.x, titleXY.y, runningApplet);
                buffer.setFont(new Font("Terminal", Font.BOLD, FONTSIZE));
                buffer.setColor(Color.WHITE);
                if (playMouseOver) {
                    buffer.setColor(Color.RED);
                }

                buffer.drawString("Play", playXY.x, playXY.y);
                buffer.setColor(Color.WHITE);
                buffer.setFont(new Font("Terminal", Font.BOLD, 20));
                int spacing = 24;
                buffer.drawString("Captain! We are caught in a Fractoid field. These dangerous", playXY.x + 190, playXY.y);
                buffer.drawString("asteroids contain a highly unstable element known as", playXY.x + 190, playXY.y + spacing);
                buffer.drawString("FRACTONIUM-238. We must be careful when using our laser", playXY.x + 190, playXY.y + spacing * 2);
                buffer.drawString("to destroy them, we need to use the exact amount of power", playXY.x + 190, playXY.y + spacing * 3);
                buffer.drawString("to destroy them or the energy will destabilize the fractoid", playXY.x + 190, playXY.y + spacing * 4);
                buffer.drawString("causing an explosion powerful enough to destroy the ship!", playXY.x + 190, playXY.y + spacing * 5);
                buffer.drawString("Thankfully some helpful aliens have left marks on them", playXY.x + 190, playXY.y + spacing * 6);
                buffer.drawString("indicating how much energy is required. Convert the percen-", playXY.x + 190, playXY.y + spacing * 7);
                buffer.drawString("-tage of laser power into a fraction matching the marking.", playXY.x + 190, playXY.y + spacing * 8);
                buffer.drawString("If you can't work out the power. Then avoid the Fractoid", playXY.x + 190, playXY.y + spacing * 9);
                buffer.drawString("at all costs.", playXY.x + 190, playXY.y + spacing * 10);

                buffer.drawString("Controls: ", playXY.x - 150, playXY.y + 50);
                buffer.drawString("W/Up Arrow -- Move ship up", playXY.x - 140, playXY.y + 80);
                buffer.drawString("S/Down Arrow -- Move ship down", playXY.x - 140, playXY.y + 110);
                buffer.drawString("A/Left Arrow -- Decrease power", playXY.x - 140, playXY.y + 140);
                buffer.drawString("D/Right Arrow -- Increase power", playXY.x - 140, playXY.y + 170);
                buffer.drawString("Spacebar -- Fire laser", playXY.x - 140, playXY.y + 200);
                buffer.drawString("M -- Toggle music", playXY.x - 140, playXY.y + 240);
    }
    
    /**
     * This method draws the actual game play screen to the buffer. Including
     * the ship, health and power bars, and Fractoids. And if an explosion has
     * just occurred then flash the screen white.
     */
    public void drawGameScreen(){
        ship.draw(buffer);

            drawFractoids();
            drawLasers();
            drawTopUI();
            drawLeftUI();
            drawLowerUI();
            if (explosion) {
                buffer.fillRect(0, 0, 1000, 600);
                explosion = false;
            }
    }

    /**
     *  Draws the top portion of the gameplay UI. Including the life icons, and 
     *  level text
     */
    public void drawTopUI() {
        int gap = 5;
        for (int i = 0; i < lives; i++) {
            AffineTransform af = new AffineTransform();
            af.translate(gap, 5);
            af.scale(0.25, 0.25);
            buffer.drawImage(lifeImage, af, runningApplet);
            gap += (lifeImage.getWidth(runningApplet) / 4) + 5;
        }
        buffer.drawString("Level: " + level, gap, 25);
    }

    /**
     * Draws the left portion of the game play UI consisting solely of the two
     * health rectangles
     */
    public void drawLeftUI() {
        buffer.setColor(Color.GREEN);
        buffer.drawRoundRect(healthOutline.x, healthOutline.y,healthOutline.width,healthOutline.height,20,20);
        buffer.fillRoundRect(healthRectangle.x,healthRectangle.y,healthRectangle.width,healthRectangle.height,20,20);
        buffer.setColor(Color.WHITE);
    }

    /**
     * Draws the lower portion of the game play UI. Which includes the power 
     * of the laser as either a decimal or percentage depending on the level.
     * Then draws the two power rectangles and finally the score
     */
    public void drawLowerUI() {
        buffer.setColor(Color.ORANGE);
        buffer.drawRoundRect(powerOutline.x,powerOutline.y,powerOutline.width,powerOutline.height, 20,20);
        buffer.fillRoundRect(powerRectangle.x,powerRectangle.y,powerRectangle.width,powerRectangle.height,20,20);
        buffer.setColor(Color.WHITE);
        buffer.setFont(new Font("Terminal", Font.BOLD, 24));
        if (level % 2 == 1) {
            buffer.drawString("Power Level: " + powerRectangle.width / 5 + "%", 10, 595);
        } else {
            buffer.drawString("Power Level: 0." + powerRectangle.width / 5, 10, 595);
        }

        buffer.drawString("Score: " + score, (int) powerOutline.getMaxX() + 10, (int) powerOutline.getMaxY());
    }

    /**
     * Draws all the {@link Laser}s currently in the lasers list to the buffer
     */
    public void drawLasers() {
        for (int i = 0; i < lasers.size(); i++) {
            lasers.get(i).draw(buffer);
        }
    }
    /**
     * Draws all of the {@link Fractoid}'s in the ArrayList to the buffer
     */
    public void drawFractoids() {
        for (Fractoid fractoid : fractoids) {
            fractoid.draw(buffer);
        }
    }
    /**
     * Draws all of the {@link Asteroid}s in the ArrayList to the buffer
     */
    public void drawAsteroids() {
        for (Asteroid asteroid : asteroids) {
            asteroid.draw(buffer);
        }
    }
    /**
     * Checks for a collision between a {@link Laser} and a {@link Fractoid}
     * If the hitBoxes intersect then check if the Laser's power level is equal
     * to the Fractoid's rounded answer. If they are then remove them from their
     * respective lists and spawn smaller asteroids in place of the Fractoid. If
     * the power level was correct then play the explosion and increase the score.
     * If not play a different explosion and remove some health.
     */
    public void checkCollision() {
        try {
            int fracs = 0;
            int lase = 0;
            //Loop through all the Fractoids
            while (fracs < fractoids.size()) {
                //Loop through all the Lazers
                while (lase < lasers.size()) {
                    //Check if the two hitBoxes intersect
                    if (lasers.get(lase).getHitBox().intersects(fractoids.get(fracs).getHitBox()) || fractoids.get(fracs).getHitBox().intersects(lasers.get(lase).getHitBox())) {
                        
                        //Spawn up from 4 - 8 smaller asteroids
                        Random rand = new Random();
                        int spawn = rand.nextInt(5) + 4;
                        //Add Asteroids to the list
                        for (int i = 0; i < spawn; i++) {
                            asteroids.add(new Asteroid(asteroidImages.get(rand.nextInt(asteroidImages.size())), new Point((int)(fractoids.get(fracs).getPosX() + fractoids.get(fracs).getHitBox().width/4), (int)(fractoids.get(fracs).getPosY() + fractoids.get(fracs).getHitBox().height/4)), rand.nextInt(4) +4, rand.nextInt(6), rand.nextBoolean()));
                        }
                        
                        
                        //If the power level waa correct
                        if (fractoids.get(fracs).getQuestion().compareTo(new Fraction(lasers.get(lase).getPowerLevel())) == 0) {
                            score += (EASY * level) * 20;
                            fractoidExplosion.play();

                        } else { // The power level was wrong
                            if (score - 50 > 0) { //Prevent negative scores
                                score -= 50;
                            } else {
                                score = 0;
                            }
                            badPower.play();
                            explosion = true; //Flashes screen white
                            healthRectangle.height -= 100;
                            healthRectangle.y += 100;
                        }
                        //Remove the Dractoid and Lazer
                        lasers.remove(lase);
                        fractoids.remove(fracs);
                        lase--;
                        fracs--;
                    }
                    lase++;
                }
                fracs++;
            }

        } catch (IndexOutOfBoundsException e) {
            System.out.println("IOOR Exception from checkCollision()");
        }
    }

    /**
     * Checks whether the ships hitBox intersects with any of the Fractoids hitBoxes
     * If it does reduce the health bar significantly and play the ship explosion sound
     */
    public void checkShipFractoidCollision() {
        for (int i = 0; i < fractoids.size(); i++) {
            if (fractoids.get(i).getHitBox().intersects(ship.getHitBox())) {
                fractoids.remove(i);
                healthRectangle.y += 200;
                healthRectangle.height -= 200;
                shipCollision.play();
            }
        }
    }

    /**
     * Loop through every Laser and updates its position. If the Laser is off 
     * screen then remove it
     */
    public void updateLasers() {
        for (int i = 0; i < lasers.size(); i++) {
            lasers.get(i).updatePosition();
            if (lasers.get(i).getPosition().getX() > 1000) {
                lasers.remove(i);
            }
        }
    }

    /**
     * Generates Asteroids randomly with a 1/10 chance each tick. Using a random
     * image for the Asteroid and adding it to the Asteroids list.
     */
    public void addAsteroid() {
        Random rand = new Random();
        if (rand.nextInt(10) == 1) {
            int imageNum = rand.nextInt(asteroidImages.size());
            asteroids.add(new Asteroid(asteroidImages.get(imageNum), new Point(runningApplet.getWidth() + 500, rand.nextInt(runningApplet.getHeight() + 50) - 50), rand.nextInt(8) + 2));
        }
    }

    

    /**
     * Loops through all the Fractoids and updates their positions and checks if
     * they are off screen and removes them if so
     */
    public void updateFractoids() {
        for (int i = 0; i < fractoids.size(); i++) {
            fractoids.get(i).updatePosition();
            if (fractoids.get(i).getHitBox().getMaxX() < 0) {
                fractoids.remove(i);
            }
        }
    }


    /**
     * Loops through all the Asteroids and updates their positions and checks if
     * they are off screen and removes them if so
     */
    public void updateAsteroids() {
        for (int i = 0; i < asteroids.size(); i++) {
            asteroids.get(i).updatePosition();
            if (asteroids.get(i).getPosX() < -400) {
                asteroids.remove(i);
            }
        }
    }

    /**
     * Checks if the health bars height is less than zero. If so reset it and 
     * decrease lives by one if lives is greater than zero. Reduces the score
     */
    public void updateHealth() {
        if (healthRectangle.height <= 0) {
            if (lives > 0) {

                lives--;
                healthRectangle = new Rectangle(10, 60, 20, 500);
                if (score - 500 > 0) { //Prevent negative scores
                    score -= 500;
                } else {
                    score = 0;
                }
            }
        }
    }

    /**
     * Updates the players level if the players score is greater than the level 
     * squared multiplied by 1000
     */
    public void updateLevel() {
        if (score >= Math.pow(level, 2) * 1000) {
            level++;
            fractoids.clear();
            count = 290;
        }
    }

    /**
     * Checks if the player is out of lives and has no more health. If so then 
     * set the current screen to be the game over screen
     */
    public void checkForGameOver() {
        if (lives <= 0 && healthRectangle.height <= 0 && currentScreen == Screen.Game) {
            currentScreen = Screen.GameOver;
        }
    }
    
    /**
     * Checks if a given keycode is in the list of pressed keys
     * @param keyCode The keycode that is being checked
     * @return True if the key with that keycode is pressed, false if not
     */
    private boolean isKeyPressed(int keyCode) {
        return keysPressed.get(keyCode);
    }
    
    /**
     * Checks whether or not any of the game control keys are pressed. If yes 
     * perform that keys corresponding action
     */
    private void pressedKeysCheck() {

        if (isKeyPressed(KeyEvent.VK_W) || isKeyPressed(KeyEvent.VK_UP)) {
            if (ship.getPosY() > 50) {
                //Move ship up
                ship.setPosition(new Point((int) ship.getPosX(), (int) ship.getPosY() - 5));
            }
        }
        if (isKeyPressed(KeyEvent.VK_DOWN) || isKeyPressed(KeyEvent.VK_S)) {

            if (ship.getMaxY() < 575) {
                //Move ship down
                ship.setPosition(new Point((int) ship.getPosX(), (int) ship.getPosY() + 5));
            }
        }
        if (isKeyPressed(KeyEvent.VK_LEFT) || isKeyPressed(KeyEvent.VK_A)) {
            if (currentScreen == Screen.Game && powerRectangle.width > 0 && count % 4 == 0) {
                //Decrease laser power
                powerRectangle.width -= 25;
            }
        }
        if (isKeyPressed(KeyEvent.VK_RIGHT) || isKeyPressed(KeyEvent.VK_D)) {
            if (currentScreen == Screen.Game && powerRectangle.width < 500 && count % 4 == 0) {
                //Increase Laser power
                powerRectangle.width += 25;
            }
        }
        if (isKeyPressed(KeyEvent.VK_SPACE)) {
            if (!ship.getCooldown() && currentScreen == Screen.Game && powerRectangle.width > 0) {
                //Fire a Laser
                fireLaser.play();
                lasers.add(new Laser(new Point((int) ship.getPosX() + 200, (int) ship.getPosY() + 30), laserImage, powerRectangle.width / 5));
                countDown = 50;
            }
        }
        if (isKeyPressed(KeyEvent.VK_M)) {
            //Toggle background music
            if (music) {
                backgroundLoop.stop();
                music = false;
            } else {
                backgroundLoop.loop();
                music = true;
            }
        }
    }
    
    /**
     * THis method performs the games logic on a timer tick from the running applet
     * @param e The event that triggered this method
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        pressedKeysCheck();
        runningApplet.showStatus(String.valueOf(fractoids.size()));
        Random rand = new Random();
        //Increase the counter
        if (currentScreen == Screen.Game) {
            count++;
        }

        //If the counter is greater than 300 then make a new Fractoid
        if (count > 300) {
            fractoids.add(new Fractoid(asteroidImages.get(rand.nextInt(asteroidImages.size())), new Point(runningApplet.getWidth() + 500, rand.nextInt(runningApplet.getHeight() - asteroidImages.get(0).getHeight(null))), 1 * level, EASY));
            count = 0;
        }
        countDown--; //Decrease the laser countdown
        checkForGameOver();
        addAsteroid();
        updateAsteroids();
        updateFractoids();
        updateLasers();
        checkCollision();
        checkShipFractoidCollision();
        updateHealth();
        updateLevel();
        ship.setCooldown(countDown);
        //Everything bellow here relates to the parrallax background
        if (spaceX + background.getWidth(runningApplet) > 0) {
            spaceX -= spaceMove;
        } else {
            spaceX = background.getWidth(runningApplet) - 2;
        }

        if (spaceX1 + background.getWidth(runningApplet) > 0) {
            spaceX1 -= spaceMove;
        } else {
            spaceX1 = background.getWidth(runningApplet) - 2;
        }

        if (starsX + starField.getWidth(runningApplet) > 0) {
            starsX -= starsMove;
        } else {
            starsX = starField.getWidth(runningApplet);
        }
        if (starsX1 + starField.getWidth(runningApplet) > 0) {
            starsX1 -= starsMove;
        } else {
            starsX1 = starField.getWidth(runningApplet);
        }
        runningApplet.repaint();
    }

    /**
     * Unused
     * @param e 
     */
    @Override
    public void keyTyped(KeyEvent e) {
    }
    /**
     * This method adds the pressed key to the bitset of pressed keys
     * @param e The event that triggered this method
     */
    @Override
    public void keyPressed(KeyEvent e) {
        keysPressed.set(e.getKeyCode());
    }

    /**
     * This method removes a pressed key from the bitset of pressed keys
     * @param e The event that triggered this method
     */
    @Override
    public void keyReleased(KeyEvent e) {
        keysPressed.clear(e.getKeyCode());
    }
    
    /**
     * Unused
     * @param e 
     */
    @Override
    public void mouseDragged(MouseEvent e) {
    }
    
    /**
     * This method is used to check if the user is mousing over the Play
     * or Play Again buttons
     * @param e The event that triggered this method
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        if (currentScreen == Screen.Main) {
            if (playRectangle.contains(e.getPoint())) {
                playMouseOver = true;
            } else {
                playMouseOver = false;
            }
        } else {
            if (currentScreen == Screen.GameOver) {
                if (playAgainRectangle.contains(e.getPoint())) {
                    playAgainMouseOver = true;
                } else {
                    playAgainMouseOver = false;
                }
            }
        }
    }

    /**
     * Checks if the mouse is in the Menu screen and if the mouse is over the
     * play button, if yes then change the current screen to gane. If the current
     * screen is game over it checks if the user clicked play again if so it 
     * changes the screen to the menu
     * @param e The event that triggered this method
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (currentScreen == Screen.Main) {
            if (playRectangle.contains(e.getPoint())) {
                currentScreen = Screen.Game;
                lives = 3;
            }

        } else {
            if (currentScreen == Screen.GameOver) {
                if (playAgainRectangle.contains(e.getPoint())) {
                    score = 0;
                    level = 1;
                    count = 0;
                    countDown = 0;
                    lasers.clear();
                    fractoids.clear();
                    currentScreen = Screen.Main;
                }
            }
        }

    }

    /**
     * Unused
     * @param e 
     */
    @Override
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Unused
     * @param e 
     */
    @Override
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Unused
     * @param e 
     */
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Unused
     * @param e 
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }

   /**
    * This enumeration is used to easily identify which screen the game is
    * currently on
    */
    private enum Screen {

        Main, Game, GameOver
    }
}
