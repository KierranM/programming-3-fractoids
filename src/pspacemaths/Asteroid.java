package pspacemaths;

import java.awt.*;
import java.awt.geom.*;

/**
 * THis class defines the asteroids that are seen through Fractoids
 * @author Kierran McPherson
 */
public class Asteroid {
    /**
     * A point representing the X & Y coordinates of the top left of the asteroid
     */
    private Point2D position;
    /**
     * The Image of the asteroid
     */
    private Image image;
    /**
     * The amount of pixels the Asteroid moves horizontally
     */
    private int xSpeed;
    /**
     * The amount of pixels the Asteroid moves vertically
     */
    private int ySpeed;
    /**
     * A boolean representing whether or not the Asteroid is moving up or down 
     * on the Y axis
     */
    private boolean negativeY;

    /**
     * Constructs a new Asteroid with a given image, position, and horizontal speed
     * @param image The image of this asteroid
     * @param position A point containing the X & Y values representing the 
     * location on the screen of the Asteroid
     * @param XSpeed The speed in pixels the Asteroid moves each update along the X axis
     */
    public Asteroid(Image image, Point position, int XSpeed) {
        this.image = image;
        this.position = position;
        this.xSpeed = XSpeed;
        this.ySpeed = 0;
        negativeY = false;
    }
    
    /**
     * Constructs a new Asteroid with a given image, position,horizontal speed, 
     * vertical speed, and whether or not it is moving up or down
     * @param image The image of the Asteroid
     * @param position The starting Point of the Asteroid
     * @param XSpeed The speed in pixels the Asteroid moves each update along the X axis
     * @param ySpeed The speed in pixels the Asteroid moves each update along the Y axis
     * @param negativeY A boolean that determines whether or not the Asteroid is moving up or down on the Y axis
     */
    public Asteroid(Image image, Point position, int XSpeed, int ySpeed, boolean negativeY){
        this.image = image;
        this.position = position;
        this.xSpeed = XSpeed;
        this.ySpeed = ySpeed;
        this.negativeY = negativeY;
    }

    /**
     * Gets the Asteroid's image
     * @return The Image of the Asteroid
     */
    public Image getImage() {
        return image;
    }

    /**
     * Gets the Point2D of the Asteroid
     * @return The Asteroids Point2D position
     */
    public Point2D getPosition() {
        return position;
    }

    /**
     * Sets the Asteroid's position
     * @param position The Asteroids new Point position
     */
    public void setPosition(Point position) {
        this.position = position;
    }
    
    /**
     * Gets the Asteroid's Position on the X axis. Convenience method instead of using getPosition().getX()
     * @return The X value of the Asteroids position
     */
    public double getPosX(){
        return position.getX();
    }
    
    /**
     * Gets the Asteroid's position on the Y axis. Convenience method instead of using getPosition().getY()
     * @return The Y value of the Asteroids position
     */
    public double getPosY(){
        return position.getY();
    }
    
    /**
     * Updates the Asteroid's position by increasing the X value by the xSpeed.
     * And either decreases or increases the Y value by the ySPeed
     * depending on the value of neagtiveY
     */
    public void updatePosition(){
        int x = (int)getPosX() - getxSpeed();
        int y = (int)getPosY();
        if (negativeY) {
            y -= getySpeed();
        }
        else{
            y += getySpeed();
        }
        position = new Point(x,y);
    }

    /**
     * Gets the Asteroid's  xSpeed
     * @return The Asteroid's horizontal speed in pixels
     */
    public int getxSpeed() {
        return xSpeed;
    }

    /**
     * Gets the Asteroid's ySpeed
     * @return THe Asteroid's vertical speed in pixels
     */
    public int getySpeed() {
        return ySpeed;
    }
    
    
    /**
     * Draws the Asteroid to the given buffer, with the correct AffineTransform 
     * and scaled based on its speed
     * @param buffer The buffer the asteroid should be drawn to.
     */
    public void draw(Graphics2D buffer){
        AffineTransform af = new AffineTransform();
            af.translate(getPosX(), getPosY());
            af.scale(getxSpeed() * 0.08, getxSpeed() * 0.08);
            buffer.drawImage(getImage(), af, null);
    }
    
}
