package pspacemaths;

import java.awt.Graphics;
import javax.swing.*;

/**
 * This is the JApplet class of the game. It does nothing more than create an instance of {@link FractoidGame} and sets the
 * appropriate listeners before starting the timer
 * @author Kierran McPherson
 */
public class SpaceMaths extends JApplet{

    /**
     * The instance of the {@link FractoidGame}
     */
    private FractoidGame game;
    /**
     * The timer that controls all the movement in the game
     */
    private Timer timer;

    /**
     * This method initialises the variables of the applet, requests focus, and adds the listeners
     */
    @Override
    public void init() {
        game = new FractoidGame(this);
        timer = new Timer(25, game);
        setFocusable(true);
        requestFocus();
        addKeyListener( game);
        addMouseMotionListener(game);
        addMouseListener(game);
        timer.start();
    }

    /**
     * Called when the applet stops. Prevents the music from playing when the applet is closed
     */
    @Override
    public void stop() {
        game.stopMusic();
    }
    /**
     * This method draws the entire game all at once
     * @param g The graphics object used to draw the game image to the screen
     */
    @Override
    public void paint(Graphics g) {
        g.drawImage(game.draw(), 0, 0, this);
    }
    /**
     * Called when the applet updates. Overridden to simply call the paint method
     * @param g The graphics device used to draw to the screen (Unused)
     */
    @Override
    public void update(Graphics g) {
        repaint();
    }
    
}
