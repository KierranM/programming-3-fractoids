/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pspacemaths;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

/**
 *  This class describes the Ship of the {@link FractoidGame}. It provides methods to draw, update, and access the fields of the ship.
 * @author Kierran McPherson
 */
public class Ship {
    /**
     * The ArrayList of images of the ship
     */
    private ArrayList<Image> images;
    /**
     * The Point representing the top left position of the ship
     */
    private Point2D position;
    /**
     * The number of images in the ArrayList
     */
    public int numberOfImages;
    /**
     * A boolean representing whether or no the ships laser is in cooldown mode
     */
    private boolean cooldown;
    /**
     * A rectangle that encases the ship and acts as a hit detection box
     */
    private Rectangle hitBox;

    /**
     * This constructs a new instance of the ship class with the given list of images and starting position
     * @param images The ArrayList of images of the ship
     * @param startingPosition The starting position of the ship
     */
    public Ship(ArrayList<Image> images, Point startingPosition) {
        position = new Point(startingPosition);
        this.images = images;
        numberOfImages = images.size();
        cooldown = false;
        hitBox = new Rectangle(startingPosition, new Dimension(images.get(0).getWidth(null)/2, images.get(0).getHeight(null)/2));
    }

    /**
     * Gets the ArrayList of images
     * @return The ArrayList of images
     */
    public ArrayList<Image> getImages() {
        return images;
    }

    /**
     * Gets the ships position
     * @return The 2D Point location of the ship
     */
    public Point2D getPosition() {
        return position;
    }
    
    /**
     * Gets the ships position on the X axis
     * @return The X coordinate of the ships position
     */
    public double getPosX(){
        return position.getX();
    }
    /**
     * Gets the ships position on the Y axis
     * @return The Y coordinates of the ships position
     */
    public double getPosY(){
        return position.getY();
    }
    
    /**
     * Gets the current value of the cooldown variable
     * @return The value of cooldown
     */
    public boolean getCooldown(){
        return cooldown;
    }
    
    /**
     * Gets the width of the ships image
     * @return An integer representing the width of the ship
     */
    public int getWidth(){
        return images.get(0).getWidth(null);
    }
    /**
     * Gets the height of the ships image
     * @return An integer representing the height of the ship
     */
    public int getHeight(){
        return images.get(0).getHeight(null);
    }

    /**
     * Sets the cooldown given an integer. If the integer is greater than or equal to zero then the ship is still in cooldown
     * @param countdown The integer that is used to check if the ship is still in a cooldown state
     */
    public void setCooldown(int countdown) {
        if (countdown >= 0) {
            cooldown = true;
        }
        else{
            cooldown = false;
        }
    }
    
    /**
     * This draws the ship to the given Graphics2D buffer using the correct AffineTransformation
     * @param buffer The Graphics2D object that the ship is being drawn to
     */
    public void draw(Graphics2D buffer){
        AffineTransform shipAF = new AffineTransform();
            shipAF.translate(getPosX(), getPosY());
            shipAF.scale(0.5, 0.5);
            buffer.drawImage(getRandomImage(), shipAF, null);
    }
    
    
    /**
     * Gets a single Image from the ArrayList of images given an integer index. Convenience method instead of using getImages.get().
     * @param index The integer index of the image you want to get
     * @return The image at the given index or null if that index is out of range
     */
    public Image getImage(int index){
        if(index < images.size()){
            return images.get(index);
        }
        else{
            return null;
        }
    }

    /**
     * Gets the rectangle representing the ships hitbox
     * @return A rectangle that is the same size as the ship
     */
    public Rectangle getHitBox() {
        return hitBox;
    }

    /**
     * Sets the ArrayList of images to the given ArrayList
     * @param images An ArrayList of images
     */
    public void setImages(ArrayList<Image> images) {
        this.images = images;
        numberOfImages = images.size();
    }

    /**
     * Sets the position of the ship to the given point. Moves the hitBox rectangle to that point
     * @param position The Point2D that will become the ships new position
     */
    public void setPosition(Point2D position) {
        this.position = position;
       hitBox =  new Rectangle((Point)position, new Dimension(images.get(0).getWidth(null)/2, images.get(0).getHeight(null)/2));
    }
    /**
     * Gets a random Image from the ArrayList of images
     * @return A random Image
     */
    public Image getRandomImage(){
        Random rand = new Random();
        return images.get(rand.nextInt(numberOfImages));
    }
    
    /**
     * Gets the maximum X value of the ships hitBox. A convenience method instead of using getHitBox().getMaxX()
     * @return The maximum value of the X access of the hitBox
     */
    public double getMaxX(){
        return hitBox.getMaxX();
    }
    
    /**
     * Gets the maximum Y value of the ships hitBox. A convenience method instead of using getHitBox().getMaxY()
     * @return The maximum value of the Y access of the hitBox
     */
    public double getMaxY(){
        return hitBox.getMaxY();
    }
}
